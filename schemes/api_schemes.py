from cerberus import Validator


class HelpAPISchemes:
    GET_HELP_FOR_QUEST = {
        "isPaid": {'type': 'boolean'},
        "cost": {'type': 'integer'},
        "level": {'type': 'integer'},
        "id": {'type': 'integer'}
    }


class QuestAPISchemes:
    GET_HELP_FOR_QUEST = {
        'id': {'type': 'integer'},
        'game_id': {'type': 'integer'},
        'name': {'type': 'string', 'min': 2, 'max': 40, 'required': True},
        'type': {'type': 'string'},
        'chest': {'type': 'string', 'required': True},
        'location': {'type': 'string', 'max': 500, 'required': True},
        'long': {'type': "number", 'min': -90, 'max': 90, 'required': True},
        'lat': {'type': "number", 'min': -180, 'max': 180, 'required': True},
        'radius': {'type': "integer", 'min': 100, 'max': 1000},
        'qr': {'type': 'string'},
        'color': {'type': 'string'},
        'faked_long': {'type': "number"},
        'faked_lat': {'type': "number"}
    }


def validate_scheme(expected_scheme, actual_scheme, all_required=None):
    validator = Validator(expected_scheme, required_all=all_required)
    is_valid = validator.validate(actual_scheme)
    return is_valid, validator.errors
