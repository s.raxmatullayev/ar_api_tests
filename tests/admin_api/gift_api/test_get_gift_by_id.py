from tests.admin_api.gift_api.base_test import BaseTest
import http


class TestGetGiftByID(BaseTest):

    def test_get_gift_by_id(self):
        response = self.response
        assert response.status_code == http.HTTPStatus.CREATED, f"Something went wrong with {
            response.text}"

        response = self.client.get_gift_by_id(response.object_id)
        assert response.status_code == http.HTTPStatus.OK, f"Something went wrong with {
            response.text}"

        actual_gift = response.object_model
        assert actual_gift == self.gift
