from tests.admin_api.gift_api.base_test import BaseTest
from models.gift import Gift
import http


class TestCreateGift(BaseTest):

    def test_create_gift(self):
        response = self.response
        assert response.status_code == http.HTTPStatus.CREATED, \
            f"Here is what went wrong {response.text}"

        response = self.client.get_gift_by_id(response.object_id)
        assert response.status_code == http.HTTPStatus.OK

        actual_gift = response.object_model
        assert actual_gift == self.gift
