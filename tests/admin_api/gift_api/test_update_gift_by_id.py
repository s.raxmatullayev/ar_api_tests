from models.gift import Gift
from tests.admin_api.gift_api.base_test import BaseTest
import http


class TestUpdateGiftById(BaseTest):
    def test_update_gift_by_id(self):
        response = self.response
        gift_id = response.object_id
        assert response.status_code == http.HTTPStatus.CREATED, f"Something went wrong with {
            response.text}"

        updated_gift = Gift.get_random_gift()
        response = self.client.update_gift_by_id(
            gift_id, updated_gift.__dict__)
        assert response.status_code == http.HTTPStatus.OK

        response = self.client.get_gift_by_id(gift_id)
        assert response.status_code == http.HTTPStatus.OK, f"Something went wrong with {
            response.text}"

        actual_gift = response.object_model
        assert actual_gift == updated_gift
