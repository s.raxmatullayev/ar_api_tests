from tests.admin_api.gift_api.base_test import BaseTest
import http


class TestGetAllGifts(BaseTest):

    def test_get_all_gifts(self):
        response = self.client.get_all_gifts()
        assert response.status_code == http.HTTPStatus.OK
