from clients.admin_api_client.gift_api_client import GiftApiClient
from models.gift import Gift


class BaseTest:
    client = None
    response = None
    gift = None

    @classmethod
    def one_time_set_up(cls, access_token):
        cls.client = GiftApiClient(access_token)
        cls.gift = Gift.get_random_gift()

    def setup_method(self):
        self.response = self.client.create_gift(self.gift.__dict__)

    def teardown_method(self):
        self.client.delete_gift_by_id(self.response.object_id)
