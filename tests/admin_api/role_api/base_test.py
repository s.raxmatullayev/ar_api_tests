from clients.admin_api_client.role_api_client import RoleApiClient
from models.role import Role


class BaseTest:
    client = None
    response = None
    role = None

    @classmethod
    def one_time_set_up(cls, access_token):
        cls.client = RoleApiClient(access_token)
        cls.role = Role.get_random_role()

    def setup_method(self):
        self.response = self.client.create_role(self.role.__dict__)

    def teardown_method(self):
        self.client.delete_role_by_id(self.response.object_id)


   
