import pytest

from tests.admin_api.role_api.base_test import BaseTest
from models.role import Role
import http


class TestCreateRole(BaseTest):

    def test_create_role(self):
        response = self.response
        assert response.status_code == http.HTTPStatus.CREATED, f"Here is what went wrong {
            response.text}"
        role_id = response.object_id

        response = self.client.get_role_by_id(role_id)
        assert response.status_code == http.HTTPStatus.OK

        actual_role = response.object_model
        assert actual_role == self.role
