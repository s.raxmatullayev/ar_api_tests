from tests.admin_api.role_api.base_test import BaseTest
import http


class TestGetAllRoles(BaseTest):

    def test_get_all_roles(self):
        response = self.client.get_all_roles()
        assert response.status_code == http.HTTPStatus.OK
