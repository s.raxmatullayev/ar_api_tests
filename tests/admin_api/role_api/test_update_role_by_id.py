from tests.admin_api.role_api.base_test import BaseTest
from models.role import Role
import http


class TestUpdateRoleById(BaseTest):

    def test_update_role_by_id(self):
        role = Role.get_random_role()
        role_id = self.response.object_id

        response = self.client.update_role_by_id(role_id, role.__dict__)
        assert response.status_code == http.HTTPStatus.OK

        response = self.client.get_role_by_id(role_id)
        assert response.status_code == http.HTTPStatus.OK

        actual_role = response.object_model
        assert actual_role == role
