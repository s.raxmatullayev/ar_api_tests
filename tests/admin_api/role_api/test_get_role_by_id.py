from tests.admin_api.role_api.base_test import BaseTest
import http


class TestGetRoleByID(BaseTest):

    def test_get_role_by_id(self):
        response = self.client.get_role_by_id(self.response.object_id)
        assert response.status_code == http.HTTPStatus.OK, f"Something went wrong with {
            response.text}"

        actual_role = response.object_model
        assert actual_role == self.role
