import pytest
import http
from tests.admin_api.user_api.base_test import BaseTest
from models.user import User


class TestCreateUser(BaseTest):

    def test_create_user(self):
        response = self.response
        assert response.status_code == http.HTTPStatus.CREATED, f"Here is what went wrong {response.text}"

        response = self.client.get_user_by_id(response.object_id)
        assert response.status_code == http.HTTPStatus.OK, f"Here is what went wrong {response.text}"

        actual_user = response.object_model
        assert actual_user == self.user
