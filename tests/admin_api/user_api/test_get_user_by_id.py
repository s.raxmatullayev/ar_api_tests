from tests.admin_api.user_api.base_test import BaseTest
import http


class TestGetUserByID(BaseTest):

    def test_get_user_by_id(self):
        response = self.client.get_user_by_id(self.response.object_id)
        assert response.status_code == http.HTTPStatus.OK, f"Something went wrong with {
            response.text}"

        actual_user = response.object_model
        assert actual_user == self.user
