from models.user import User
from tests.admin_api.user_api.base_test import BaseTest
import http


class TestUpdateUserById(BaseTest):
    updated_user = User.get_random_user()

    def test_update_user_by_id(self):
        user_id = self.response.object_id

        response = self.client.get_user_by_id(user_id)
        assert response.status_code == http.HTTPStatus.OK, f"Here is what went wrong {
            response.text}"

        response = self.client.update_user_by_id(
            user_id, self.updated_user.__dict__)
        assert response.status_code == http.HTTPStatus.OK, f"Here is what went wrong {
            response.text}"

        response = self.client.get_user_by_id(user_id)
        assert response.status_code == http.HTTPStatus.OK, f"Here is what went wrong {
            response.text}"

        actual_user = response.object_model
        assert actual_user == self.updated_user
