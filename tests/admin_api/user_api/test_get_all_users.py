from tests.admin_api.user_api.base_test import BaseTest
import http


class TestGetAllUsers(BaseTest):
    def test_get_all_user(self):
        response = self.client.get_all_users()
        assert response.status_code == http.HTTPStatus.OK
