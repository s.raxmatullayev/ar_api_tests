import pytest
from clients.admin_api_client.user_api_client import UserApiClient
from models.user import User


class BaseTest:
    client = None
    response = None
    user = None

    @classmethod
    def one_time_set_up(cls, access_token):
        cls.client = UserApiClient(access_token)
        cls.user = User.get_random_user()

    def setup_method(self):
        self.response = self.client.create_user(self.user.__dict__)

    def teardown_method(self):
        self.client.delete_user_by_id(self.response.object_id)
