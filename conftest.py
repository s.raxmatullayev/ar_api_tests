import pytest
import requests

from models.user_tokens import UserTokens
from config import config


@pytest.fixture(scope="session")
def login():
    headers = {
        "accept": "application/json",
        "x-tenant-id": config.X_TENANT_ID,
        "Content-Type": "application/x-www-form-urlencoded"
    }
    request_body = {
        "password": config.PASSWORD,
        "username": config.USERNAME
    }
    url = config.API_URL + "/auth/login"
    response = requests.post(url=url, headers=headers, data=request_body)
    dictionary = response.json()
    tokens = UserTokens
    tokens.access_token = f"{dictionary['data']['access_token']}"
    tokens.refresh_token = f"{dictionary['data']['refresh_token']}"
    return tokens


@pytest.fixture(scope="session", autouse=True)
def before_tests(request, login):
    access_token = login.access_token
    seen = {None}
    session = request.node
    for item in session.items:
        cls = item.getparent(pytest.Class)
        if cls not in seen:
            if hasattr(cls.obj, "one_time_set_up"):
                cls.obj.one_time_set_up(access_token)
            seen.add(cls)
