from config import config
from utils.api_requests import APIRequests


class BaseClient:
    def __init__(self, access_token):
        self.headers = {
            "Authorization": "Bearer " + access_token,
            "accept": "application/json",
            "x-tenant-id": config.X_TENANT_ID
        }
        self.requests = APIRequests()
        self.base_url = config.API_URL
