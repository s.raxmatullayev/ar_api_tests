from clients.admin_api_client.admin_api_client import AdminClient
from models.user import User


class UserApiClient(AdminClient):

    def __init__(self, access_token):
        super().__init__(access_token, "users")

    def get_all_users(self):
        response = super()._get_all_items()
        return response

    def get_user_by_id(self, role_id):
        response = super()._get_item_by_id(role_id)
        user = User.from_json(response.as_dict['data'])
        response.object_model = user
        return response

    def create_user(self, payload):
        response = super()._create_item(payload)
        user_id = response.as_dict['data']['id']
        response.object_id = user_id
        return response

    def update_user_by_id(self, user_id, payload):
        response = super()._update_item_by_id(user_id, payload)
        return response

    def delete_user_by_id(self, user_id):
        response = super()._delete_item_by_id(user_id)
        return response
