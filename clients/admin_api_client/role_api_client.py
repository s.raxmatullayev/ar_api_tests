from clients.admin_api_client.admin_api_client import AdminClient
from models.role import Role


class RoleApiClient(AdminClient):

    def __init__(self, access_token):
        super().__init__(access_token, "roles")

    def get_all_roles(self):
        response = super()._get_all_items()
        return response

    def get_role_by_id(self, role_id):
        response = super()._get_item_by_id(role_id)
        role = Role.from_json(response.as_dict['data'])
        response.object_model = role
        return response

    def create_role(self, payload):
        response = super()._create_item(payload)
        role_id = response.as_dict['data']['id']
        response.object_id = role_id
        return response

    def update_role_by_id(self, role_id, payload):
        response = super()._update_item_by_id(role_id, payload)
        return response

    def delete_role_by_id(self, role_id):
        response = super()._delete_item_by_id(role_id)
        return response
