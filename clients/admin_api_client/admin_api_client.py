from clients.base_client import BaseClient


class AdminClient(BaseClient):

    def __init__(self, access_token, endpoint):
        self.endpoint = endpoint
        super().__init__(access_token)

    def _get_all_items(self):
        url = f"{self.base_url}/admin/{self.endpoint}"
        response = self.requests.get(url, self.headers)
        return response

    def _get_item_by_id(self, item_id):
        url = f"{self.base_url}/admin/{self.endpoint}/{item_id}"
        response = self.requests.get(url, self.headers)
        return response

    def _create_item(self, payload):
        url = f"{self.base_url}/admin/{self.endpoint}"
        response = self.requests.post(url, payload, self.headers)
        return response

    def _update_item_by_id(self, item_id, payload):
        url = f"{self.base_url}/admin/{self.endpoint}/{item_id}"
        response = self.requests.put(url, payload, self.headers)
        return response

    def _delete_item_by_id(self, item_id):
        url = f"{self.base_url}/admin/{self.endpoint}/{item_id}"
        response = self.requests.delete(url, self.headers)
        return response
