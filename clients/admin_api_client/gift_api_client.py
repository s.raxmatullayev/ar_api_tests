from clients.admin_api_client.admin_api_client import AdminClient
from models.gift import Gift


class GiftApiClient(AdminClient):
    def __init__(self, access_token):
        super().__init__(access_token, "gifts")

    def get_all_gifts(self):
        response = super()._get_all_items()
        return response

    def create_gift(self, payload):
        response = super()._create_item(payload)
        content = response.as_dict
        response.object_id = content['data']['id']
        return response

    def get_gift_by_id(self, gift_id):
        response = super()._get_item_by_id(gift_id)
        gift = Gift.from_json(response.as_dict['data'])
        response.object_model = gift
        return response

    def update_gift_by_id(self, gift_id, payload):
        response = super()._update_item_by_id(gift_id, payload)
        return response

    def delete_gift_by_id(self, gift_id):
        response = super()._delete_item_by_id(gift_id)
        return response
