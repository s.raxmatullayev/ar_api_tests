from dataclasses import dataclass


@dataclass
class Quest:
    id: int
    name: str
    type: str
    chest: str
    long: float
    lat: float
    rad: float
    radius: float
    state: str
    qr: str
    color: str
    faked_long: float
    faked_lat: float

    def __post_init__(self):
        if not -90 <= self.long <= 90:
            raise ValueError("Invalid real longitude")
        if not -180 <= self.lat <= 180:
            raise ValueError("Invalid real latitude")
        if not -90 <= self.long <= 90:
            raise ValueError("Invalid fake longitude")
        if not -180 <= self.lat <= 180:
            raise ValueError("Invalid fake latitude")
