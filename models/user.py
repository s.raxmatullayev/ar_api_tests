import json
from dataclasses import dataclass

from models.base_model import BaseModel
from utils.randomizer import generate_radom_email, generate_random_set, get_random_string


@dataclass
class User(BaseModel):
    first_name: str = None
    last_name: str = None
    email: str = None
    phone_number: str = None
    password: str = None
    is_active: bool = None
    role_id: int = None
    created_at: str = None
    id: int = None
    username: str = None
    photo: str = None
    password_hash: str = None

    def __eq__(self, other):
        first_name = self.first_name == other.first_name
        last_name = self.last_name == other.last_name
        email = self.email == other.email
        phone_number = self.phone_number == other.phone_number
        role_id = self.role_id == other.role_id
        return first_name and last_name and email and email and phone_number and role_id

    @classmethod
    def get_random_user(cls):
        phone_number = "977771727"
        return User(first_name=get_random_string(10), role_id=2, last_name=get_random_string(15), email=generate_radom_email(), phone_number=phone_number, username=phone_number, is_active=True, password=get_random_string(10))
