from dataclasses import dataclass
from models.base_model import BaseModel

from utils.randomizer import get_random_string


@dataclass
class Gift(BaseModel):
    name: str
    description: str
    id: int = None
    image: str = None

    def __eq__(self, other):
        return self.name == other.name and self.description == other.description

    @classmethod
    def get_random_gift(cls):
        return Gift(get_random_string(10), get_random_string(20))
