from dataclasses import dataclass


@dataclass
class HelpForQuest:
    is_paid: bool
    cost: int
    level: int
    id: int

    def __post_init__(self):
        if not 0 <= self.level <= 5:
            raise ValueError("Invalid level")

    def as_help_for_quest(dct):
        return HelpForQuest(is_paid=dct['isPaid'], cost=dct['cost'], level=dct['level'], id=dct['id'])
