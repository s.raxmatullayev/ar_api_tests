from dataclasses import dataclass

from models.base_model import BaseModel
from utils.randomizer import get_random_string


@dataclass
class Role(BaseModel):
    name: str
    id: int = None

    def __eq__(self, other):
        return self.name == other.name

    @classmethod
    def get_random_role(cls):
        return Role(name=get_random_string(10))
