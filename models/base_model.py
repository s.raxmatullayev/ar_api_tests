import json


class BaseModel:
    @classmethod
    def from_json(cls, data):
        json_string = json.dumps(data)
        json_dict = json.loads(json_string)
        return cls(**json_dict)
    
