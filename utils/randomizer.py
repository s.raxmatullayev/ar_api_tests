import random
import string


def get_random_string(length):
    # Generate a random string of fixed length
    letters = string.ascii_letters
    result_str = ''.join(random.choice(letters) for i in range(length))
    return result_str


def generate_random_set(n, start, end):
    return "998".join(str(random.randint(start, end)) for _ in range(n))


def generate_radom_email():
    return get_random_string(5) + '@gmail.com'
