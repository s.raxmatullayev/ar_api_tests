from dataclasses import dataclass
import requests


@dataclass
class Response:
    status_code: int
    text: str
    as_dict: object
    headers: dict
    object_model: object = None
    object_id: int = None


def _get_responses(response):
    status_code = response.status_code
    text = response.text
    try:
        as_dict = response.json()
    except ValueError:
        as_dict = {}

    headers = response.headers
    return Response(status_code, text, as_dict, headers)


class APIRequests:

    def get(self, url, headers):
        response = requests.get(url, headers=headers)
        return _get_responses(response)

    def post(self, url, payload, headers):
        response = requests.post(url, data=payload, headers=headers)
        return _get_responses(response)

    def delete(self, url, headers):
        response = requests.delete(url, headers=headers)
        return _get_responses(response)

    def put(self, url, payload, headers):
        response = requests.put(url, data=payload, headers=headers)
        return response
