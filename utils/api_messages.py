class HelpMessages:
    UNPAID_HELP = "Quest Help has not yet been paid!"


class QuestMessages:
    NON_EXISTENT_GAME_QUEST = "GameQuest don't exists"
    ALREADY_WON_QUEST = "Somebody won this quest!"
